﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147  ZD 100886 End--%>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.en_ViewUserDetails" %>
<script language="javascript" type="text/javascript">
    function ClosePopUp()
    {   
        parent.document.getElementById("viewHostDetails").style.display = 'none';
        parent.document.getElementById("ViewSchedulerDetails").style.display = 'none'; //ALLDEV-857
        return false;
    }
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
	<tr>
		<td align="center">
            <table cellpadding="2" cellspacing="1" style="border-color:Black;border-width:1px;border-style:Solid; background-color:#E1E1E1;"  class="tableBody" align="center" width="50%"> <%--ZD 100426--%>
              <tr>
                <td class="subtitleblueblodtext" align="center" colspan="3">
                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, UserDetails%>" runat="server"></asp:Literal><br />
                </td>            
              </tr>
              <tr><%--FB 2579 Start--%>
               <td align="left" style="width:40%" class="blackblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Name%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrName" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ManageUserProfile_EmailID%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ManageUserProfile_Label4%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLogin" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <% if(Session["timezoneDisplay"].ToString() == "1") { %> 
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, ReportDetails_TimeZone%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrTimeZone" runat="server"></asp:Label>
               </td>
              </tr>
              <% } else {%>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, TimezoneDisplay%>" runat="server"></asp:Literal> </td>
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, Off%>" runat="server"></asp:Literal></td>
              </tr>
              <% }%>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ManageUserProfile_PrefLang%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrLang" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageUserProfile_emaillang%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrEmailLang" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ManageUserProfile_LblBlockEmails%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrBlockedEmail" runat="server"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageUserProfile_Label2%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrWork" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr>
              <tr>
               <td align="left" class="blackblodtext"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ManageUserProfile_Label3%>" runat="server"></asp:Literal> </td> 
               <td style="width:1px; vertical-align:top"><b>:</b>&nbsp;</td>
               <td align="left">
                   <asp:Label ID="lblUsrCell" runat="server" Text="N/A"></asp:Label>
               </td>
              </tr><%--FB 2579 End--%>
              <tr>
               <td align="center" colspan="3"><br />
                  <button ID="BtnUsrDetailClose" class="altMedium0BlueButtonFormat"  runat="server" onclick="javascript:return ClosePopUp();">
                  <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal></button>
               </td>
              </tr>
           </table>
		</td>
	</tr>
</table>
