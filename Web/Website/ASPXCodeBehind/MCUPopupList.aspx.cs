﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100866 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;

namespace ns_MyVRM
{
    public partial class en_MCUPopupList : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        String mcustring = "";

        #region protected Members

        protected DevExpress.Web.ASPxGridView.ASPxGridView grid;

        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("MCUPopupList.aspx", Request.Url.AbsoluteUri.ToLower());
                log = new ns_Logger.Logger();

                if (Request.QueryString["mcus"] != null)
                    mcustring = Request.QueryString["mcus"].ToString().Trim().Replace("@@", "++");

                BindMCUs();

            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }

        }
        #endregion

        #region BindMCUs
        public void BindMCUs()
        {
            String inXML1 = "";
            String outXML1 = "";
            XmlDocument xmldoc = null;
            DataTable dt = null;
            try
            {
                string activeGrp = "";
                if( Session["MCUGroupID"] != null)
                    activeGrp = Session["MCUGroupID"].ToString();


                inXML1 = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><activeGroupId>" + activeGrp + "</activeGroupId></login>";
                outXML1 = obj.CallMyVRMServer("GetVirtualBridges", inXML1, Application["COM_ConfigPath"].ToString());
                xmldoc = new XmlDocument();
                dt = new DataTable();
                if (outXML1.IndexOf("Error") <= 0)
                {
                    xmldoc.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];

                        if (!dt.Columns.Contains("ifrmDetails"))
                            dt.Columns.Add("ifrmDetails");

                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["ifrmDetails"] = dr["ID"].ToString() + "|" + dr["name"].ToString() + "|" + dr["administrator"].ToString()
                                + "|" + dr["timeZone"].ToString() + "|0|0|0";
                        }
                    }
                }

                grid.DataSource = dt;
                grid.DataBind();

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region ASPxGridView1_DataBound
        protected void ASPxGridView1_DataBound(object sender, EventArgs e)
        {
            try
            {
                String[] delimiter = { "!!" };
                String[] delimiter1 = { "||" };
                if (!IsPostBack)
                {
                    if (mcustring != "")
                    {
                        string[] partys = mcustring.Split(delimiter1, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < partys.Length; j++)
                        {
                            string temp = partys[j].ToString();
                            int tmpMCUId = 0;
                            if (temp != "")
                            {
                                for (int i = 0; i < this.grid.VisibleRowCount; i++)
                                {
                                    tmpMCUId = 0;
                                    Int32.TryParse(temp.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[0], out tmpMCUId);
                                    if (this.grid.GetRowValues(i, "id") != null)
                                    {

                                        if (Convert.ToInt32(this.grid.GetRowValues(i, "id")) == tmpMCUId)
                                        {
                                            this.grid.Selection.SelectRow(i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }
        }
        #endregion

    }
}